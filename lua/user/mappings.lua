local utils = require("user.functions")
local map = vim.keymap.set

map("i", "<C-l>", function()
  return utils.escapePair()
end)

