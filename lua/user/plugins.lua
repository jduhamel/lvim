lvim.plugins = {

  --- Color schemes ---
  "ellisonleao/gruvbox.nvim",
  "LunarVim/synthwave84.nvim",
  "roobert/tailwindcss-colorizer-cmp.nvim",
  "lunarvim/github.nvim",
  --- Telescope Extensions ---
  {
    "LukasPietzschmann/telescope-tabs",
    after = {
      "nvim-telescope/telescope.nvim",
    },
    dependencies = {
      "nvim-telescope/telescope.nvim",
    },
    config = function()
      require("telescope-tabs").setup()
    end,
  },
  --- Core Editor enhancements ---
  {
    "echasnovski/mini.nvim",
    version = false,
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "JoosepAlviste/nvim-ts-context-commentstring",
    },
    config = function()
      require("user.pkg.mini").config()
    end,
  },
  "ThePrimeaGen/Harpoon", --
  "ggandor/lightspeed.nvim",
  "windwp/nvim-spectre",
  "monaqa/dial.nvim",
  {
    "folke/noice.nvim",
    dependencies = {
      "MunifTanjim/nui.nvim",
      "rcarriga/nvim-notify",
    },
    config = function()
      require("user.pkg.noice").config()
    end,
  },
  --  { "kylechui/nvim-surround", config = function() require("nvim-surround").setup() end, },
  --- GIT ---
  --- Completions Frameworks ---
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-path",
  "hrsh7th/cmp-cmdline",
  {
    "tzachar/cmp-tabnine",
    build = "./install.sh",
    dependencies = "hrsh7th/nvim-cmp",
  },
  {
    "zbirenbaum/copilot-cmp",
    dependencies = { "zbirenbaum/copilot.lua" },
    after = { "copilot.lua" },
    config = function()
      require("copilot_cmp").setup()
    end,
  },
  {
    "zbirenbaum/copilot.lua",
    config = function()
      require("copilot").setup({
        suggestion = { enabled = false },
        panel = { enabled = false },
      })
    end,
  },
  --- TreeSitter General stuff ---
  "nvim-treesitter/playground",
  --  "nvim-treesitter/nvim-treesitter-textobjects",
  --- Language Plugins ---
  "folke/neodev.nvim",
  {
    "akinsho/flutter-tools.nvim",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function()
      require("user.pkg.flutter_tools").config()
    end
  },
  {
    "ray-x/go.nvim",
    dependencies = {
      "ray-x/guihua.lua",
      "neovim/nvim-lspconfig",
      "nvim-treesitter/nvim-treesitter",
      "mfussenegger/nvim-dap",
      "rcarriga/nvim-dap-ui",
      "theHamsta/nvim-dap-virtual-text",
      "nvim-telescope/telescope-dap.nvim",
    },
  },

  --- Org Mode ---

  -- "christianchiarulli/nvim-ts-rainbow",
  "HiPhish/nvim-ts-rainbow2", -- Configured in nvim-treesitter
  {
    "RRethy/nvim-treesitter-textsubjects",
    config = function()
      require('nvim-treesitter.configs').setup {
        textsubjects = {
          enable = true,
          prev_selection = ',', -- (Optional) keymap to select the previous selection
          keymaps = {
                ['.'] = 'textsubjects-smart',
                [';'] = 'textsubjects-container-outer',
                ['i;'] = 'textsubjects-container-inner',
          },
        },
      }
    end,
  },
  {
    -- lazy.nvim
    "chrisgrieser/nvim-various-textobjs",
    config = function()
      require("various-textobjs").setup({ useDefaultKeymaps = true })
    end,
  },
  "mfussenegger/nvim-jdtls",
}
-- JSD --  "j-hui/fidget.nvim",
-- JSD --  "christianchiarulli/nvim-ts-autotag",
-- JSD --  "kylechui/nvim-surround",
-- JSD --  "christianchiarulli/harpoon",
-- JSD --  "MattesGroeger/vim-bookmarks",
-- JSD --  "NvChad/nvim-colorizer.lua",
-- JSD --  "ghillb/cybu.nvim",
-- JSD --  "moll/vim-bbye",
-- JSD --  "folke/todo-comments.nvim",
-- JSD --  "f-person/git-blame.nvim",
-- JSD --  "ruifm/gitlinker.nvim",
-- JSD --  "mattn/vim-gist",
-- JSD --  "mattn/webapi-vim",
-- JSD --  "folke/zen-mode.nvim",
-- JSD --  "lvimuser/lsp-inlayhints.nvim",
-- JSD --  "lunarvim/darkplus.nvim",
-- JSD --  "lunarvim/templeos.nvim",
-- JSD --  "kevinhwang91/nvim-bqf",
-- JSD --  "is0n/jaq-nvim",
-- JSD --  -- "hrsh7th/cmp-emoji",
-- JSD --  "ggandor/leap.nvim",
-- JSD --  "nacro90/numb.nvim",
-- JSD --  "TimUntersberger/neogit",
-- JSD --  "sindrets/diffview.nvim",
-- JSD --  "simrat39/rust-tools.nvim",
-- JSD --  "olexsmir/gopher.nvim",
-- JSD --  "leoluz/nvim-dap-go",
-- JSD --  "mfussenegger/nvim-dap-python",
-- JSD --  "jose-elias-alvarez/typescript.nvim",
-- JSD --  "mxsdev/nvim-dap-vscode-js",
-- JSD --  "petertriho/nvim-scrollbar",
-- JSD --  "renerocksai/telekasten.nvim",
-- JSD --  -- "renerocksai/calendar-vim",
-- JSD --  {
-- JSD --    "saecki/crates.nvim",
-- JSD --    version = "v0.3.0",
-- JSD --    dependencies = { "nvim-lua/plenary.nvim" },
-- JSD --    config = function()
-- JSD --      require("crates").setup {
-- JSD --        null_ls = {
-- JSD --          enabled = true,
-- JSD --          name = "crates.nvim",
-- JSD --        },
-- JSD --      }
-- JSD --    end,
-- JSD --  },
-- JSD --  "MunifTanjim/nui.nvim",
-- JSD --  "jackMort/ChatGPT.nvim",
-- JSD --  {
-- JSD --    "jinh0/eyeliner.nvim",
-- JSD --    config = function()
-- JSD --      require("eyeliner").setup {
-- JSD --        highlight_on_key = true,
-- JSD --      }
-- JSD --    end,
-- JSD --  },
-- JSD --  { "christianchiarulli/telescope-tabs", branch = "chris" },
-- JSD --  "monaqa/dial.nvim",
-- JSD --  {
-- JSD --    "0x100101/lab.nvim",
-- JSD --    build = "cd js && npm ci",
-- JSD --  },
-- JSD --  -- { "tzachar/cmp-tabnine", build = "./install.sh" },
-- JSD --  {
-- JSD --    "zbirenbaum/copilot.lua",
-- JSD --    -- event = { "VimEnter" },
-- JSD --    config = function()
-- JSD --      vim.defer_fn(function()
-- JSD --        require("copilot").setup {
-- JSD --          plugin_manager_path = os.getenv "LUNARVIM_RUNTIME_DIR" .. "/site/pack/packer",
-- JSD --        }
-- JSD --      end, 100)
-- JSD --    end,
-- JSD --  },
-- JSD --  {
-- JSD --    "zbirenbaum/copilot-cmp",
-- JSD --    after = { "copilot.lua" },
-- JSD --    config = function()
-- JSD --      require("copilot_cmp").setup {
-- JSD --        formatters = {
-- JSD --          insert_text = require("copilot_cmp.format").remove_existing,
-- JSD --        },
-- JSD --      }
-- JSD --    end,
-- JSD --  },
-- JSD --  -- "MunifTanjim/nui.nvim",
-- JSD --  -- {
-- JSD --  --   "folke/noice.nvim",
-- JSD --  --   event = "VimEnter",
-- JSD --  --   config = function()
-- JSD --  --     require("noice").setup()
-- JSD --  --   end,
-- JSD --  -- },
-- JSD --
-- JSD --  -- https://github.com/jose-elias-alvarez/typescript.nvim
-- JSD --  -- "rmagatti/auto-session",
-- JSD --  -- "rmagatti/session-lens"
-- JSD --}
